
const express = require('express');
const cors = require('cors');

const routes = require('./routes/routes');
const verifyToken = require('./helpers/AuthMiddleware');

global.__basePath = __dirname;

const app = express();
app.use(cors());
app.use(express.json());
// app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
    res.status(200).json({ 'insureMe-Test' : 'SudeshD'});
});

app.use(verifyToken);
app.use(routes);



//handle  invalid request URL
app.use((req, res, next) => {
    let err = Error('Invalid Request URL');
    err.code = 404;
    next(err);
});


// Handle all errors
app.use((error, req, res, next) => {

    let js = {
        status: 'INVALID REQUEST',
        message: error.message,
        log: 'NOT HANDLED'
    };

    res.status(404).json(js);
});

module.exports = app;



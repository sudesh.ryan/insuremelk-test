'use strict';

const dotEnv = require('dotenv').config();

const http = require('http');
const db_connection = require('./database/dbConnect');
const PORT = process.env.NODE_PORT || 6000;

db_connection()
    .then((mongoose) => {

        const closeDB = () => {
            mongoose.disconnect();
            console.log('Server exiting..');
        };

        process.on('SIGINT', closeDB);
        process.on('SIGTERM', closeDB);
        process.on('exit', closeDB);


        const App = require('./app');
        const server = http.createServer(App);

        server.listen(PORT, () => {
            console.log(`Server listening at port: ${PORT}`);
        }); 
        
    })
    .catch((err) => {

        console.log('Database connection error...');
        console.log(err);

    });
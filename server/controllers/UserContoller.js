const fs = require('fs');

const responses = require('../helpers/responses');
const User = require('../models/User');
const ExcelJS = require('exceljs');

class UserController {
    constructor() {
        this.UserRoles = ['Admin', 'Manager', 'Team Lead', 'Agent'];
        // this.UserRoles = ['Agent', 'Team Lead', 'Manager', 'Admin'];
        /* this.UserRoleRanks = {
            'Admin': 3,
            'Manager': 2,
            'Team Lead': 1,
            'Agent': 0,
        }; */

        this.Operaions = {
            GET: 'get',
            CREATE: 'create',
            UPDATE: 'update',
            DELETE: 'delete',
        };

        const Op = this.Operaions;
        this.Permissions = {
            'Admin': { role: 'Admin', ops: [Op.GET, Op.CREATE, Op.UPDATE, Op.DELETE] },
            'Manager': { role: 'Manager', ops: [Op.GET, Op.CREATE, Op.UPDATE, Op.DELETE] },
            'Team Lead': { role: 'Team Lead', ops: [Op.GET, Op.CREATE, Op.UPDATE, Op.DELETE] },
            'Agent': { role: 'Agent', ops: [] },
        };
    }

    /**
     * Return if a given user role is allowed to perform given operation
     *
     * @param userRole Role of the user
     * @param op Operation from this.Operaions
     */
    isPermitted(userRole, op) {
        return this.Permissions[userRole].ops.includes(op);
    }

    /**
     * Return array of user roles that a logged in user can operate on
     *
     * @param userRole User role
     */
    getPermittedRoles(userRole) {
        let roles = [];
        switch (userRole) {
            case 'Admin':
                roles = ['Admin', 'Manager', 'Team Lead', 'Agent'];
                break;

            case 'Manager':
                roles = ['Manager', 'Team Lead', 'Agent'];
                break;

            case 'Team Lead':
                roles = ['Agent'];
                break;

            case 'Agent':
                roles = [];
                break;
        }

        return roles;
    }

    /**
     * Return all users with pagination support
     *
     * body params
     *  - pagination.from
     *  - pagination.count
     */
    async getAll(req, res) {
        if (!this.isPermitted(req.authUser.role, this.Operaions.GET)) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        let pagination = req.body.pagination;
        const permittedRoles = this.getPermittedRoles(req.authUser.role);
        const output = {};

        try {
            let query = User.find({}).where('role').in(permittedRoles).sort('createdAt');

            if (pagination && pagination.from && pagination.count) {
                query = query.skip(pagination.from).limit(pagination.count);
                output.from = pagination.from;
            }
            let users = await query.exec();
            output['count'] = users.length;
            // output["total"] = await User.estimatedDocumentCount().exec();
            output['users'] = users.map((u) => u.toObj());

            return responses.successResponse(res, output);
        } catch (err) {
            return responses.errorResponse(res, err);
        }
    }

    /**
     * Return the user by Id
     *
     */
    async getById(req, res) {
        if (!this.isPermitted(req.authUser.role, this.Operaions.GET)) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        const permittedRoles = this.getPermittedRoles(req.authUser.role);
        const userId = req.params.id;

        try {
            let user = await User.findById(userId).exec();
            user = user ? user.toObj() : {};

            if (permittedRoles.includes(user.role) === false) {
                // Check permission auth user's role
                // Ex: Team Lead can only update Agent users, etc..
                return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
            }

            return responses.successResponse(res, user);
        } catch (err) {
            return responses.errorResponse(res, err);
        }
    }

    /**
     * Create a new user and save to DB
     *
     * Body params:
     *  - name
     *  - email
     *  - password
     *  - dob
     *  - role
     *  - department
     */
    async create(req, res) {
        if (!this.isPermitted(req.authUser.role, this.Operaions.CREATE)) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        // const deps = ['Engineering', 'HR', 'Finance', 'Marketing'];
        const permittedRoles = this.getPermittedRoles(req.authUser.role);
        const input = req.body;

        if (Object.keys(input).length < 6) {
            return responses.errorResponse(res, 'Incomplete request body data', 400);
        }

        if (input.dob.length !== 10) {
            return responses.errorResponse(res, 'Inavlid Date of Birth format', 400);
        }

        // Check permission auth user's role
        // Ex: Team Lead can only create Agent users, etc..
        if (permittedRoles.includes(input.role) === false) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        try {
            const existing = await User.findOne({ email: input.email }).exec();
            if (existing) {
                return responses.errorResponse(res, 'Email address already exists');
            }

            let newUser = {
                name: input.name,
                email: input.email,
                password: input.password,
                dob: input.dob + 'T00:00:00.000Z',
                role: input.role,
                department: input.department,
            };

            newUser = await new User(newUser).save();
            return responses.successResponse(res, newUser);
        } catch (err) {
            return responses.errorResponse(res, err);
        }
    }

    /**
     * Update user record by ID
     *
     * Body params: <all optional>
     *  - name
     *  - email
     *  - password
     *  - dob
     *  - role
     *  - department
     */
    async update(req, res) {
        if (!this.isPermitted(req.authUser.role, this.Operaions.UPDATE)) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        const permittedRoles = this.getPermittedRoles(req.authUser.role);
        const userId = req.params.id;
        const input = req.body;

        // Check permission auth user's role
        // Ex: Team Lead can only update Agent users, etc..
        if (input.role && permittedRoles.includes(input.role) === false) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        try {
            let user = await User.findById(userId).exec();
            if (user === null) {
                return responses.errorResponse(res, 'No user for the given ID');
            }

            user.name = input.name || user.name;
            user.email = input.email || user.email;
            user.password = input.password || user.password;
            user.dob = input.dob || user.dob;
            user.role = input.role || user.role;
            user.department = input.department || user.department;

            if (user.isModified()) {
                user = await user.save();
            }
            return responses.successResponse(res, user.toObj());
        } catch (err) {
            return responses.errorResponse(res, err);
        }
    }

    /**
     * Delete a user by ID
     *
     * URL params:
     *  - id
     */
    async delete(req, res) {
        if (!this.isPermitted(req.authUser.role, this.Operaions.DELETE)) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        const permittedRoles = this.getPermittedRoles(req.authUser.role);
        const userId = req.params.id;

        try {
            const user = await User.findById(userId);
            if (!user) {
                return responses.errorResponse(res, 'No user for the given ID');
            } else if (permittedRoles.includes(user.role) === false) {
                // Check permission auth user's role
                // Ex: Team Lead can only update Agent users, etc..
                return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
            }

            const delCount = await User.deleteOne({ _id: userId }).exec();
            return responses.successResponse(res, delCount);
        } catch (err) {
            return responses.errorResponse(res, err);
        }
    }

    getPermittedFrontendData(req, res) {
        const data = {
            departments: ['Engineering', 'HR', 'Finance', 'Marketing'],
        };

        switch (req.authUser.role) {
            case 'Admin':
                data['roles'] = ['Admin', 'Manager', 'Team Lead', 'Agent'];
                break;

            case 'Manager':
                data['roles'] = ['Manager', 'Team Lead', 'Agent'];
                break;

            case 'Team Lead':
                data['roles'] = ['Agent'];
                break;
        }

        return responses.successResponse(res, data);
    }

    async downloadUsersAsExcel(req, res) {
        if (req.authUser.hasRole('Admin') === false) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        try {
            let allUsers = await User.find({}).sort('createdAt').exec();
            allUsers = allUsers.map((u) => {
                let dob = u.dob.toISOString();
                dob = dob.slice(0, dob.indexOf('T'));
                return [u.name, u.email, dob, u.role, u.department];
            });

            let today = new Date();
            today = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
            const fileName = 'ALL_USERS_' + today + '.xlsx';
            const filePath = __basePath + '/temp/' + fileName;

            // PREPARE EXCEL FILE //
            const workbook = new ExcelJS.Workbook();
            const ws = workbook.addWorksheet('ALL USERS');
            ws.columns = [
                { header: 'Name', width: 40 },
                { header: 'Email', width: 40 },
                { header: 'DOB', width: 20 },
                { header: 'Role', width: 20 },
                { header: 'Department', width: 20 },
            ];
            ws.addRows(allUsers);
            ws.getRow(1).font = { bold: true }; // set column name row bold
            await workbook.xlsx.writeFile(filePath);
            // ---------------- //

            return res.download(filePath, fileName);
        } catch (err) {
            return responses.errorResponse(res, err);
        }
    }

    async uploadUsersFromExcel(req, res) {
        if (req.authUser.hasRole('Admin') === false) {
            return responses.errorResponse(res, 'You are not permitted to perform this operaion', 402);
        }

        if(!req.file) {
            return responses.errorResponse(res, 'No file has been uploaded');
        }

        const file = req.file;
        const filePath = __basePath + '/' + file.path;

        try {
            const workbook = new ExcelJS.Workbook();
            await workbook.xlsx.readFile(filePath);
            const ws = workbook.worksheets[0];

            // GET EXCEL ROWS
            let userRows = [];

            ws.eachRow(function (row, rowNumber) {
                if (rowNumber === 1) return; // first row is column headers

                userRows.push(row.values);
            });

            /* 
              row.values gives a array with first item always null, like this:
                    [null,"uploaded user 1","uploaded_user1@insureme.test","1980-12-31","Admin","Engineering"],
            */

            // VALIDATE
            let userRowsValidated = [];
            for (const user of userRows) {
                let skip = false;
                if (user.length !== 6) continue; // must be only 5 columns in excel file + null added by ExcelJS

                for (let i = 1; i < user.length; i++) {
                    if (user[i] instanceof Date) continue; // date values come as Date() objects

                    if (typeof user[i] !== 'string') skip = true; // all items must be strings, no excel formatting
                }
                if (!user[2].toString().match(/\S+@\S+\.\S+/)) skip = true; // validate email

                if (skip) continue;

                const dob = user[3] instanceof Date ? user[3].toISOString() : user[3] + 'T00:00:00.000Z';
                userRowsValidated.push({
                    name: user[1],
                    email: user[2],
                    password: '',
                    dob: dob,
                    role: user[4],
                    department: user[5],
                });
            }

            if(userRowsValidated.length === 0) {
                return responses.errorResponse(res, 'No valid data rows found. Excel file content may not be in correct format.');
            }

            // ADDING TO DB:
            //  - If email exists, then update record. Otherwise create new record
            const result = { total: userRows.length, validated: userRowsValidated.length, created: 0, updated: 0 };

            // Update exisitng emails first and remove from array
            for (const item of userRowsValidated) {
                const existingUser = await User.findOne({ email: item.email }).exec();
                if (existingUser) {
                    existingUser.name = item.name;
                    existingUser.dob = item.dob;
                    existingUser.role = item.role;
                    existingUser.department = item.department;
                    await existingUser.save();
                    result.updated += 1;
                    item.updated = true;
                }
            }

            // filter out updated items
            userRowsValidated = userRowsValidated.filter((item) => !item.updated);

            // Rest are new users
            if (userRowsValidated.length > 0) {
                await User.insertMany(userRowsValidated, { lean: true });
                result.created = userRowsValidated.length;
            }

            fs.unlinkSync(filePath);
            return responses.successResponse(res, result);
        } catch (err) {
            fs.unlinkSync(filePath);
            return responses.errorResponse(res, err);
        }
        
    }
}

module.exports = new UserController();

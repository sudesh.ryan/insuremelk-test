const responses = require("../helpers/responses");
var jwt = require('jsonwebtoken');

const User = require("../models/User");

class AuthController {
    /**
     * Return jwt token on successfull login
     *
     * Body params:
     *  - email
     *  - password
     */
    async login(req, res) {
        const SECRET = process.env.APP_KEY || 'JIUzI1NiIsInR5cCI6';
        const { email:userEmail, password:userPass } = req.body;

        try {
            const dbUser = await User.findOne({ email: userEmail }).exec();

            if(!dbUser) { // No db record
                return responses.errorResponse(res, 'Authentication Failed. Check email.', 401);
            }

            if(dbUser.role === 'Agent') { // No db record
                return responses.errorResponse(res, 'Authentication Failed. Agents are not allowed to login.', 401);
            }

            if(userPass === dbUser.password) {
                // Auth OK. Send token
                const payload = { userId: dbUser._id };
                const token = jwt.sign(payload, SECRET, { expiresIn: '24h' });
                const out = {
                    user: dbUser.toObj(),
                    authToken: token
                }

                return responses.successResponse(res, out);
            }
            else {
                return responses.errorResponse(res, 'Authentication Failed. Check password.', 401);
            }



        } catch(err) {
            return responses.errorResponse(res, err);
        }

    }

    /**
     * Validate jwt token. If it is valid, return auth user data
     */
    async validateToken(req, res) {
        // If it hits this function, token is already valid
        // Response with auth user data

        return responses.successResponse(res, { tokenValid: true, user: req.authUser });
    }
}

module.exports = new AuthController();

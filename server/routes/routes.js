const router = require('express').Router();
const multer  = require('multer');

const multipart = multer({ dest: 'temp' })

const userController = require('../controllers/UserContoller');
const authController = require('../controllers/AuthController');

// Auth routes
router.post('/login', authController.login);
router.post('/validate-token', authController.validateToken);


// Users routes
router.get('/users', userController.getAll.bind(userController));
router.get('/users/frontend-data', userController.getPermittedFrontendData.bind(userController));
router.get('/users/download-xlsx', userController.downloadUsersAsExcel.bind(userController));
router.get('/users/:id', userController.getById.bind(userController));
router.post('/users', userController.create.bind(userController));
router.post('/users/upload-xlsx', multipart.single('user_list_excel'), userController.uploadUsersFromExcel.bind(userController));
router.put('/users/:id', userController.update.bind(userController));
router.delete('/users/:id', userController.delete.bind(userController));


module.exports = router;
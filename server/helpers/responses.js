const resCodes = {
    200: 'SUCCESS',
    201: 'SUCCESS',
    400: 'ERROR',
    401: 'NOT AUTHENTICATED',
    402: 'NOT PERMITTED',
    404: 'NOT FOUND',
    500: 'SERVER ERROR',
};

exports.errorResponse = (res, err, statusCode = 400) => {
    if (typeof err != 'string') {
        console.log('ERROR:\n');
        console.log(err);
        console.log('\n');
    }

    res.status(200).json({
        status: resCodes[statusCode],
        message: typeof err === 'object' ? err.message : err,
        error: err,
    });
};

exports.successResponse = (res, data, statusCode = 200) => {
    res.status(200).json({
        status: 'SUCCESS',
        data: data,
    });
};

var jwt = require("jsonwebtoken");

const responses = require("./responses");
const User = require("../models/User");

/**
 * Validate JWT token on AuthToken header
 */
let verifyToken = async function (req, res, next) {
    const exclude = ['/', '/login'];
    if (exclude.includes(req.path)) {
        return next();
    }

    const SECRET = process.env.APP_KEY || "JIUzI1NiIsInR5cCI6";
    const token = req.header("auth-token");

    if (!token) {
        return responses.errorResponse(res, "Token Authentication Failed. Please Sign in again.", 401);
    }

    // Verify jwt. If verified, create authUser object and attach to req for later use
    try {
        let payload = jwt.verify(token, SECRET);
        const user = await User.findById(payload.userId).exec();

        const authUser = user.toObj();
        authUser.hasRole = function (role) {
            return role === this.role;
        };
        req.authUser = Object.freeze(authUser);
        // console.log('is admin: ' + req.authUser.hasRole('Admin'));

        next();

    } catch (err) {
        err.message += 'Please Sign in again.';
        return responses.errorResponse(res, err, 401);
    }
};

module.exports = verifyToken;

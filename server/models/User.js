const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
    {
        name: { type: String, required: true },
        email: { type: String, required: true },
        password: { type: String },
        dob: { type: mongoose.Schema.Types.Date, required: true },
        role: {
            type: String,
            required: true,
            default: "Agent",
            enum: ["Admin", "Manager", "Team Lead", "Agent"],
        },
        department: { type: String, required: true },
        // created_at: { type: mongoose.Schema.Types.Date, default: Date.now() },
        // updated_at: { type: mongoose.Schema.Types.Date },
    },
    { timestamps: true }
);

UserSchema.methods.toObj = function () {
    let dob = this.dob.toISOString();
    dob = dob.slice(0, dob.indexOf('T'));

    return {
        id: this._id.toString(),
        name: this.name,
        email: this.email,
        dob: dob,
        role: this.role,
        department: this.department,
        createdAt: this.createdAt,
        updatedAt: this.updatedAt,
    };
};

UserSchema.methods.hasRole = function (role) {
    return role === this.role;
};

const User = mongoose.model("User", UserSchema);

module.exports = User;

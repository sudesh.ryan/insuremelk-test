const mongoose = require("mongoose");
const faker = require("faker");

const User = require("../models/User");
const deps = ['Engineering', 'HR', 'Finance', 'Marketing'];

/**
 * Seed users with fake data
 */
async function seedUsers() {
    console.log('Seeding Users...start');

    // Admin
    const admin = new User({
        name: "Admin",
        email: "admin@insureme.test",
        password: "admin",
        dob: new Date('2000-01-01'),
        role: "Admin",
        department: deps[0],
    });
    await admin.save();

    // Manager
    const manager = new User({
        name: "Manager",
        email: "manager@insureme.test",
        password: "manager",
        dob: new Date('2000-01-01'),
        role: "Manager",
        department: deps[0],
    });
    await manager.save();

    // Admin
    const teamLead = new User({
        name: "Team Lead",
        email: "team_lead@insureme.test",
        password: "team_lead",
        dob: new Date('2000-01-01'),
        role: "Team Lead",
        department: deps[0],
    });
    await teamLead.save();

    let fakeUsers = [];
    for(let i = 1; i< 50; i++) {
        const name = faker.name.findName();
        let dob = faker.date.between('1980-01-01', '2000-12-31');
        const month = (dob.getMonth() + 1) < 10 ? '0' + (dob.getMonth() + 1) : dob.getMonth() + 1; 
        const date = dob.getDate() < 10 ? '0' + dob.getDate() : dob.getDate(); 
        dob = dob.getFullYear() + '-' + month + '-' + date + 'T00:00:00.000Z';
        const u = {
            name: name,
            email: name.toLocaleLowerCase().replace(' ', '.') + '@insureme.test',
            password: "12345",
            dob: dob,
            role: "Agent",
            department: faker.random.arrayElement(deps),
        };
        fakeUsers.push(u);
    }

    await User.insertMany(fakeUsers);
    
    console.log('Seeding Users...end');
}

const db_connection = require('./dbConnect');
db_connection()
    .then(async (mongoose) => {

        await seedUsers();
        mongoose.disconnect();

    })
    .catch((err) => {

        console.log('Database connection error...Seeding Failed');
        console.log(err);

    });

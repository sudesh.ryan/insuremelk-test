const dotEnv = require('dotenv').config();
const mongoose = require('mongoose');

module.exports = async () => {

    try {
        const monogouri = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`;
        const options = {   
            authSource: 'admin'
        };
        let conn = await mongoose.connect(monogouri, options);
                                                                            
        console.log('MongoDB Connected.');

        mongoose.connection.on('error', (err) => {
            console.log("MongoDB Connection Error\n");
            console.error(err);
        });

        return mongoose;
    }
    catch (err) {
        return Promise.reject(err);
    }
}

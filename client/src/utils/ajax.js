import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:9999';
// axios.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
axios.interceptors.request.use(function (config) {
    if(!config.headers['Content-Type']) {
        config.headers['Content-Type'] = 'application/json; charset=utf-8';
    }
    config.headers['auth-token'] = getAuthToken();

    return config;
});

function getAuthToken() {
    return window.localStorage.getItem('authToken') || '';
}

// -- APP REQUESTS -- //

export async function signInUser(data) {
    let res = null;
    try {
        data = { email: data.email, password: data.password };
        res = await axios.post('/login', data);
        return res;
    } catch (err) {
        console.log('AJAX ERROR: login()');
        throw err;
    }

}

export async function createUser(data) {
    let res = null;
    try {
        /* data = { 
            name: data.name, 
            email: data.email, 
            password: data.password,
            dob: data.dob,
            role: data.role,
            department: data.department
        }; */
        res = await axios.post('/users', data);
        return res;
    } catch (err) {
        console.log('AJAX ERROR: createUser()');
        throw err;
    }
}

export async function getFrontendData() {
    try {
        const res = await axios.get('/users/frontend-data');
        return res;
    } catch (err) {
        console.log('AJAX ERROR: getFrontendData()');
        throw err;
    }

}


export async function getAllUsers() {
    try {
        const res = await axios.get('/users');
        return res;
    } catch (err) {
        console.log('AJAX ERROR: getAllUsers()');
        throw err;
    }

}

export async function getUserById(userId) {
    try {
        const res = await axios.get('/users/' + userId);
        return res;
    } catch (err) {
        console.log('AJAX ERROR: getUserById()');
        throw err;
    }

}

export async function deleteUser(userId) {
    try {
        const res = await axios.delete('/users/' + userId);
        return res;
    } catch (err) {
        console.log('AJAX ERROR: deleteUser()');
        throw err;
    }

}

export async function updateUser(userId, data) {
    try {
        const res = await axios.put('/users/' + userId, data);
        return res;
    } catch (err) {
        console.log('AJAX ERROR: updateUser()');
        throw err;
    }

}

export async function uploadUsersExcel(formData) {
    try {
        const res = await axios.post('/users/upload-xlsx', formData);
        return res;
    } catch (err) {
        console.log('AJAX ERROR: uploadUsersExcel()');
        throw err;
    }

}

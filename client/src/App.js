import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import 'antd/dist/antd.css';

import Dashboard from './components/dashboard/Dashboard';
import Login from './components/Login';
import Base from './components/Base';

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    <Route path="/login" component={Login} />
                    <Route path="/dashboard" component={Dashboard} />
                    <Route path="/" component={Base} />
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;

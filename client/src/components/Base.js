import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

export default function Base() {
    const isAuthed = useSelector((state) => state.isAuthenticated);
    
    return <Redirect to={isAuthed ? '/dashboard' : '/login'}></Redirect>;
}

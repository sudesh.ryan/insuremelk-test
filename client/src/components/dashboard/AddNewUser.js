import { React } from 'react';
import { useSelector } from 'react-redux';
import { Form, Input, Button, Select, DatePicker, Row, Col, Typography, message } from 'antd';
import { createUser } from '../../utils/ajax';

const { Title } = Typography;

export default function AddNewUserForm() {
    const [addNewUserForm] = Form.useForm();

    function onSubmit(values) {
        values.dob = values.dob.format('YYYY-MM-DD');
        createUser(values)
            .then((res) => {
                if (res.data.status === 'SUCCESS') {
                    addNewUserForm.resetFields();
                    message.success('New user saved successfully');
                } else {
                    // data.status = 'ERROR' || 'NOT AUTHENTICATED' || 'NOT PERMITTED'
                    console.log(res.data);
                    message.error(res.data.message);
                }
            })
            .catch((err) => {
                console.error(err);
                message.error(err.message, 5);
            });
    }

    let allowedRoles = useSelector((state) => state.permittedRoles);
    allowedRoles = allowedRoles.map((role, index) => (
        <Select.Option key={index} value={role}>
            {role}
        </Select.Option>
    ));

    let departments =  useSelector((state) => state.departments);
    departments = departments.map((dep, index) => (
        <Select.Option key={index} value={dep}>
            {dep}
        </Select.Option>
    ));

    return (
        <div className="add-new-user-wrapper" style={{maxWidth: '500px'}}>
            <Row>
                <Col>
                    <Title level={2}>Add New User</Title>
                </Col>
            </Row>

            <Row>
                <Col flex="auto">
                    <Form
                        form={addNewUserForm}
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 16,
                        }}
                        layout="horizontal"
                        labelAlign="left"
                        onFinish={onSubmit}
                        style={{border: '1px solid #ccc', padding:'2rem 1rem 0 1rem', borderRadius:'8px'}}
                    >
                        <Form.Item
                            label="Name"
                            name="name"
                            required={true}
                            rules={[
                                {
                                    required: true,
                                    message: 'Name is required',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[
                                { required: true, message: 'Email is required' },
                                { pattern: /\S+@\S+\.\S+/, message: 'Invalid Email address' },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Password is required',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            label="DOB"
                            name="dob"
                            rules={[
                                {
                                    required: true,
                                    message: 'Date of birth is required',
                                },
                            ]}
                        >
                            <DatePicker />
                        </Form.Item>

                        <Form.Item
                            label="Role"
                            name="role"
                            rules={[
                                {
                                    required: true,
                                    message: 'User Role is required',
                                },
                            ]}
                        >
                            <Select>
                                {allowedRoles}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            label="Department"
                            name="department"
                            rules={[
                                {
                                    required: true,
                                    message: 'Department is required',
                                },
                            ]}
                        >
                            <Select>
                                { departments }
                            </Select>
                        </Form.Item>

                        <Form.Item wrapperCol={{}}>
                            <Button type="primary" htmlType="submit" className="ml-auto" style={{ minWidth: '100px', display: 'block' }}>
                                Save
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </div>
    );
}

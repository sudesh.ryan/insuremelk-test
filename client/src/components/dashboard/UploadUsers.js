import { React, useState } from 'react';
import { Button, Row, Col, Divider, Upload, Alert } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { uploadUsersExcel } from '../../utils/ajax';

export default function UploadUsers() {
    const [uploadFile, setUploadFile] = useState(null);
    const [uploading, setUploading] = useState(false);
    const [uploadMsg, setUploadMsg] = useState({ status: false });

    const handleUpload = () => {
        const formData = new FormData();
        formData.append('user_list_excel', uploadFile);
        setUploading(true);

        uploadUsersExcel(formData)
            .then((res) => {
                setUploading(false);

                if (res.data.status === 'SUCCESS') {
                    const d = res.data.data;
                    setUploadMsg({
                        status: 'Upload Success',
                        description: `Total: ${d.total} | Created: ${d.created} | Updated: ${d.updated}`,
                        type: 'success',
                    });
                    setUploadFile(null);
                } else {
                    setUploadMsg({
                        status: 'Upload Unsuccessfull',
                        description: res.data.message,
                        type: 'error',
                    });
                }
            })
            .catch((err) => {
                console.log(err);
                setUploadMsg({
                    status: 'Upload Error',
                    description: err.message,
                    type: 'error',
                });
                setUploading(false);
            });
    };

    
    const authUserRole = window.localStorage.getItem('userRole');
    if (authUserRole && authUserRole !== 'Admin') {
        return (
            <div>
                <Alert
                    message="Not Permitted"
                    description="You are not permitted for this operation"
                    type="warning"
                    showIcon
                    style={{ marginTop: '2rem' }}
                />
            </div>
        );
    }

    const uploadProps = {
        maxCount: 1,
        fileList: uploadFile === null ? [] : [uploadFile],
        beforeUpload: (file) => {
            setUploadFile(file);
            return false;
        },
    };

    return (
        <div className="upload-users-wrapper">
            {/* <Row>
                <Col flex="auto">
                    <Divider orientation="left">Download All Users</Divider>
                    <Text>You can download all user list as a Excel file: </Text>
                    <Button type="primary" style={{ marginLeft: '2rem' }}>
                        Download
                    </Button>
                </Col>
            </Row> */}

            <Row style={{ marginTop: '1rem' }}>
                <Col flex="auto">
                    <Divider orientation="left">Upload Users</Divider>

                    <Upload {...uploadProps}>
                        <Button icon={<UploadOutlined />}>Select File</Button>
                    </Upload>

                    <Button type="primary" onClick={handleUpload} disabled={uploadFile === null} loading={uploading} style={{ marginTop: 16 }}>
                        {uploading ? 'Uploading' : 'Start Upload'}
                    </Button>
                </Col>
            </Row>

            <Row style={{ marginTop: '2rem' }}>
                <Col flex="auto">
                    <h4>Notes:</h4>
                    <ul>
                        <li>Excel file content cannot have any cell formatting.</li>
                        <li>First row must be column headers. Data must start from row 2.</li>
                        <li>Only columns allowed:  NAME | EMAIL | DOB | ROLE | DEPARTMENT</li>
                    </ul>
                </Col>
            </Row>

            <Row style={{ marginTop: '1rem' }}>
                <Col>
                    {uploadMsg.status && (
                        <Alert
                            message={uploadMsg.status}
                            description={uploadMsg.description}
                            type={uploadMsg.type}
                            closable
                            showIcon
                        />
                    )}
                </Col>
            </Row>
        </div>
    );
}

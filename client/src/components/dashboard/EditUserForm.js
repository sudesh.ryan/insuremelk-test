import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Form, Input, Button, Select, DatePicker, Modal, Row, Col, Space, message } from 'antd';
import moment from 'moment';

import { getUserById, updateUser } from '../../utils/ajax';

export default function EditUserForm(props) {
    const [editUserForm] = Form.useForm();
    /* const [userData, setUserData] = useState({
            name: '',
            email: '',
            password: '',
            dob: '',
            role: '',
            department: '',
    }); */

    let allowedRoles = useSelector((state) => state.permittedRoles);
    allowedRoles = allowedRoles.map((role, index) => (
        <Select.Option key={index} value={role}>
            {role}
        </Select.Option>
    ));

    let departments = useSelector((state) => state.departments);
    departments = departments.map((dep, index) => (
        <Select.Option key={index} value={dep}>
            {dep}
        </Select.Option>
    ));

    useEffect(() => {
        getUserById(props.modelData.userId)
            .then((res) => {
                if (res.data.status === 'SUCCESS') {
                    const { name, email, dob, role, department } = res.data.data;
                    editUserForm.setFieldsValue({ name, email, dob: new moment(dob), role, department, password: '' });
                } else {
                    // status = 'ERROR' || 'NOT AUTHENTICATED' || 'NOT PERMITTED'
                    console.log(res.data);
                    message.warning(res.data.message, 5);
                }
            })
            .catch((err) => {
                message.error(err.message, 5);
                console.error(err);
            });
    }, []);

    // ON FORM SUBMIT
    const onSubmit = (v) => {
        let values = Object.assign(v);
        values.dob = values.dob.format('YYYY-MM-DD');
        if (values.password === '') {
            delete values['password'];
        }

        updateUser(props.modelData.userId, values)
            .then((res) => {
                if (res.data.status === 'SUCCESS') {
                    message.success('User data updated successfully');
                    props.closeModal(true);
                } else {
                    // status = 'ERROR' || 'NOT AUTHENTICATED' || 'NOT PERMITTED'
                    console.log(res.data);
                    message.warning(res.data.message, 5);
                }
            })
            .catch((err) => {
                console.error(err);
                message.error(err.message);
            });
    };

    const handleCancel = () => {
        props.closeModal();
    };

    return (
        <Modal title="Edit User" footer={null} visible={props.modelData.visible} onCancel={handleCancel}>
            <Row justify="left">
                <Col flex="auto">
                    <Form
                        form={editUserForm}
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 16,
                        }}
                        layout="horizontal"
                        labelAlign="left"
                        onFinish={onSubmit}
                    >
                        <Form.Item
                            label="Name"
                            name="name"
                            required={true}
                            rules={[
                                {
                                    required: true,
                                    message: 'Name is required',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[
                                { required: true, message: 'Email is required' },
                                { pattern: /\S+@\S+\.\S+/, message: 'Invalid Email address' },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item label="Password" name="password">
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            label="DOB"
                            name="dob"
                            rules={[
                                {
                                    required: true,
                                    message: 'Date of birth is required',
                                },
                            ]}
                        >
                            <DatePicker />
                        </Form.Item>

                        <Form.Item
                            label="Role"
                            name="role"
                            rules={[
                                {
                                    required: true,
                                    message: 'User Role is required',
                                },
                            ]}
                        >
                            <Select>{allowedRoles}</Select>
                        </Form.Item>

                        <Form.Item
                            label="Department"
                            name="department"
                            rules={[
                                {
                                    required: true,
                                    message: 'Department is required',
                                },
                            ]}
                        >
                            <Select>{departments}</Select>
                        </Form.Item>

                        <Form.Item>
                            <Space>
                                <Button type="primary" htmlType="submit">
                                    Save
                                </Button>
                                <Button onClick={handleCancel}>Cancel</Button>
                            </Space>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </Modal>
    );
}

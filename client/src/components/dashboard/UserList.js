import { React, useState, useEffect } from 'react';
import { Row, Col, Typography, Table, Input, Space, Button, Modal, message } from 'antd';
import { DeleteFilled, EditFilled, SearchOutlined, ExclamationCircleOutlined } from '@ant-design/icons';

import { getAllUsers, deleteUser } from '../../utils/ajax';
import EditUserForm from './EditUserForm';

const { Title } = Typography;
const { confirm } = Modal;

export default function UserList() {
    const [userList, setUserList] = useState([]);

    useEffect(loadUserList, []);

    function loadUserList() {
        getAllUsers().then((res) => {
            // const data = res.data;
            if (res.data.status === 'SUCCESS') {
                const list = res.data.data.users.map((item) => {
                    return {
                        ...item,
                        key: item.id,
                    };
                });
                setUserList(list);
            } else {
                console.log(res.data);
            }
        })
        .catch((err) => {
            message.err(err.message, 5);
            console.err('UserList() \n' + err);
        });
    }

    function deleteHandler(userId) {
        function doDelete() {
            deleteUser(userId)
                .then((res) => {
                    console.log('delete ' + res.data.status);
                    if (res.data.status === 'SUCCESS') {
                        const temp = userList.filter((user) => !(user.id === userId));
                        setUserList(temp);
                        message.success('User record was deleted');
                    } else {
                        console.log(res.data);
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        }

        let user = userList.find((item) => item.id === userId);
        user = (
            <p>
                {user.name} <br />
                {user.email} <br />
                {user.role}
            </p>
        );

        confirm({
            title: 'Are you sure to delete this user?',
            icon: <ExclamationCircleOutlined />,
            content: user,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                doDelete();
            },
            onCancel() {
                console.log('Delete Cancelled');
            },
        });
    }

    // -- EDIT USER FORM -- //
    const [editUserData, setEditUserData] = useState({ visible: false, userId: '' });

    function editHandler(userID) {
        setEditUserData({ visible: true, userId: userID });
    }

    function closeEditUserModal(loadAgain = false) {
        setEditUserData({ visible: false, userId: '' });

        if(loadAgain) {
            // User was update from EditUserModal, load user list again
            loadUserList();
        }
    }

    // -- END EDIT USER FORM -- //

    // -- COLUMN FILTER DROPDOWN -- //

    const [filterSearchText, setFilterSearchText] = useState('');
    const [filterSearchedColumn, setFilterSearchedColumn] = useState('');
    const [filterSearchInput, setFilterSearchInput] = useState(null);

    function handleSearch(selectedKeys, confirm, dataIndex) {
        confirm();
        setFilterSearchText(selectedKeys[0]);
        setFilterSearchedColumn(dataIndex);
    }

    function handleReset(clearFilters) {
        clearFilters();
        setFilterSearchText('');
    }

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={(node) => setFilterSearchInput(node)}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) => (record[dataIndex] ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()) : ''),
    });

    // -- END COLUMN FILTER DROPDOWN -- //

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name.localeCompare(b.name),
            ...getColumnSearchProps('name'),
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            sorter: (a, b) => a.email.localeCompare(b.email),
            ...getColumnSearchProps('email'),
        },
        {
            title: 'DOB',
            dataIndex: 'dob',
            key: 'dob',
            sorter: (a, b) => {
                const d1 = new Date(a.dob).getTime();
                const d2 = new Date(b.dob).getTime();
                return d1 < d2 ? -1 : d1 > d2 ? 1 : 0;
            },
            ...getColumnSearchProps('dob'),
        },
        {
            title: 'Role',
            dataIndex: 'role',
            key: 'role',
            sorter: (a, b) => a.role.localeCompare(b.role),
            filters: [
                { text: 'Admin', value: 'Admin' },
                { text: 'Manager', value: 'Manager' },
                { text: 'Team Lead', value: 'Team Lead' },
                { text: 'Agent', value: 'Agent' },
            ],
            onFilter: (value, record) => record.role.indexOf(value) === 0,
        },
        {
            title: 'Department',
            dataIndex: 'department',
            key: 'department',
            sorter: (a, b) => a.department.localeCompare(b.department),
            filters: [
                { text: 'Engineering', value: 'Engineering' },
                { text: 'Marketing', value: 'Marketing' },
                { text: 'Finance', value: 'Finance' },
                { text: 'HR', value: 'HR' },
            ],
            onFilter: (value, record) => record.department.indexOf(value) === 0,
        },
        {
            title: 'Created On',
            dataIndex: 'createdAt',
            key: 'createdOn',
            render: (val) => <>{val.slice(0, val.indexOf('T'))}</>,
            sorter: (a, b) => {
                const d1 = new Date(a.createdAt).getTime();
                const d2 = new Date(b.createdAt).getTime();
                return d1 < d2 ? -1 : d1 > d2 ? 1 : 0;
            },
            ...getColumnSearchProps('createdAt'),
        },
        {
            title: '',
            key: 'action',
            align: 'right',
            width: 80,
            render: (text, record) => (
                <Space>
                    <Button shape="circle" onClick={() => editHandler(record.id)}>
                        <EditFilled />
                    </Button>
                    <Button shape="circle" danger onClick={() => deleteHandler(record.id)}>
                        <DeleteFilled />
                    </Button>
                </Space>
            ),
        },
    ];

    return (
        <>
            <div className="user-list-wrapper">
                <Row>
                    <Col>
                        <Title level={2}>User List</Title>
                    </Col>
                </Row>

                <Row>
                    <Col flex="auto">
                        <Table columns={columns} dataSource={userList} bordered /* onChange={handleChange} */ />
                    </Col>
                </Row>
            </div>

            <div>{editUserData.visible && <EditUserForm modelData={editUserData} closeModal={closeEditUserModal} />}</div>
        </>
    );
}

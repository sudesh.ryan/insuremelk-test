import { React, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { Layout, Menu, Row, Col, Dropdown, Typography } from 'antd';
import { UserOutlined, DashboardTwoTone, DashboardOutlined } from '@ant-design/icons';

import AddNewUser from './AddNewUser';
import UserList from './UserList';
import UploadUsers from './UploadUsers';

import { getFrontendData } from '../../utils/ajax';
import { permittedRolesActionSet } from '../../store/permittedRoles';
import { departmentsActionSet } from '../../store/departments';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
const { Text, Title } = Typography;

export default function Dashboard() {
    const history = useHistory();
    const dispatch = useDispatch();
    
    const isAuthed = useSelector((state) => state.isAuthenticated);
    if(!isAuthed) {
        history.replace('/login');
    }

    const Intro = () => (
        <Row>
            <Col flex="auto" style={{textAlign: 'center'}}>
                <Title>WELCOME</Title>
                <DashboardTwoTone twoToneColor="#ccc" style={{ fontSize: '20rem', marginTop: '1rem', opacity: 0.5 }} />
            </Col>
        </Row>
    );

    const userDropdownMenu = () => (
        <Menu style={{ minWidth: '150px' }} onClick={signOutUser}>
            <Menu.Item key="sign-out" icon={<UserOutlined />}>
                Sign Out
            </Menu.Item>
        </Menu>
    );

    const contentComponents = {
        'Intro': Intro,
        'AddNewUser': AddNewUser,
        'UserList': UserList,
        'UploadUsers': UploadUsers
    };

    //-- FETCH DATA AND SET STATE --//
    useEffect(() => {
        if(isAuthed === false) return;

        getFrontendData()
            .then((res) => {
                if (res.data.status === 'SUCCESS') {
                    dispatch(permittedRolesActionSet(res.data.data.roles));
                    dispatch(departmentsActionSet(res.data.data.departments));
                } else {
                    console.log(res.data);
                }
            })
            .catch((err) => {
                console.log('Error while getFrontendData()');
                console.error(err);
            }); 

    }, []);

    //- STATE -//
    const [activeContentComponent, setActiveContentComponent] = useState('Intro');

    // -- METHODS -- //

    function siderItemClick({ key }) {
        switch (key) {
            case 'users.add-new':
                setActiveContentComponent('AddNewUser');
                break;
            case 'users.list':
                setActiveContentComponent('UserList');
                break;
            case 'users.import-export':
                setActiveContentComponent('UploadUsers');
                break;
            default:
                setActiveContentComponent('Intro');
                break;
        }
    }

    function signOutUser() {
        // clear saved JWT token and redirect to login page
        window.localStorage.removeItem('authToken');
        window.localStorage.removeItem('userName');
        window.localStorage.removeItem('userRole');

        history.push('/login');
    }

    function getSignInUserName() {
        return window.localStorage.getItem('userName');
    }

    const ContentComponent = contentComponents[activeContentComponent];

    return (
        <div className="dashboard-page">
            <Layout style={{ minHeight: '100vh' }}>
                {/* Nav bar */}
                <Header className="header">
                    <Row>
                        <Col flex="auto">
                            <h2 className="title">User Management System</h2>
                        </Col>
                        <Col>
                            <div>
                                <Dropdown.Button overlay={userDropdownMenu} placement="bottomRight" icon={<UserOutlined />} size="large">
                                    <Text>{getSignInUserName()}</Text>
                                </Dropdown.Button>
                            </div>
                        </Col>
                    </Row>
                </Header>

                <Layout>
                    {/* Sidebar */}
                    <Sider width={200} className="site-layout-background">
                        <Menu mode="inline" theme="dark" onClick={(v) => siderItemClick(v)} defaultOpenKeys={['users']} style={{ height: '100%' }}>
                            <Menu.Item key="dashboard" icon={<DashboardOutlined />}>
                                Dashboard
                            </Menu.Item>

                            <SubMenu key="users" icon={<UserOutlined />} title="Users">
                                <Menu.Item key="users.add-new">Add New</Menu.Item>
                                <Menu.Item key="users.list">List</Menu.Item>
                                <Menu.Item key="users.import-export">Import/Export</Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Sider>

                    {/* Main Content */}
                    <Layout style={{ padding: '2rem' }}>
                        <Content className="content">
                            <ContentComponent />
                        </Content>
                    </Layout>
                </Layout>
            </Layout>
        </div>
    );
}

import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Form, Input, Button, Alert, message } from 'antd';

import { signInUser } from '../utils/ajax';
import { isAuthenticatedActionSet } from '../store/isAuthenticated';

export default function Login() {
    const dispatch = useDispatch();
    const history = useHistory();
    const [authFailed, setAuthFailed] = useState('');

    function onSubmit(values) {
        signInUser(values)
            .then((response) => {
                const data = response.data;

                if (data.status === 'SUCCESS') {
                    window.localStorage.setItem('authToken', data.data.authToken);
                    window.localStorage.setItem('userName', data.data.user.name);
                    window.localStorage.setItem('userRole', data.data.user.role);
                    // window.localStorage.setItem('user', JSON.stringify(data.data.user));
                    dispatch(isAuthenticatedActionSet(true));
                    history.replace('/dashboard');
                } else {
                    // status = NOT AUTHENTICATED
                    console.log(data);
                    setAuthFailed(data.message);
                    setTimeout(() => setAuthFailed(''), 5000);
                }
            })
            .catch((err) => {
                message.error(err.message);
                setAuthFailed('');
            });
    }

    return (
        <div className="login-page">
            <div className="login-form">
                <h1 style={{ textAlign: 'center' }}>InsureMe.LK Test</h1>
                <p style={{ textAlign: 'center', marginBottom: '2rem' }}>Welcome, Sign in with your credentials</p>

                {authFailed.length > 0 && <Alert message={authFailed} type="error" showIcon closable style={{ marginBottom: '2rem' }} />}

                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 16,
                    }}
                    onFinish={onSubmit}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            { required: true, message: 'Email is required' },
                            { pattern: /\S+@\S+\.\S+/, message: 'Invalid Email address' },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Password is required',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button type="primary" htmlType="submit">
                            SIGN IN
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}

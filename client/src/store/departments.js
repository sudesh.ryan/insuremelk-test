export const departmentsReducer = (state = [], action) => {
    switch (action.type) {
        case 'departmentsSet':
            return action.value;

        default:
            return state;
    }
}

export const departmentsActionSet = (value) => {
    return {
        type: 'departmentsSet',
        value
    };
};

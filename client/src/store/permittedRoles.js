export const permittedRolesReducer = (state = null, action) => {
    switch (action.type) {
        case 'permittedRolesSet':
            return action.value;

        default:
            return state;
    }
}

export const permittedRolesActionSet = (value) => {
    return {
        type: 'permittedRolesSet',
        value
    };
};

export const isAuthenticatedReducer = (state = false, action) => {
    switch (action.type) {
        case 'authenticatedSet':
            return action.value;

        default:
            return state;
    }
}

export const isAuthenticatedActionSet = (value) => {
    return {
        type: 'authenticatedSet',
        value
    };
};

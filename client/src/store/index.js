import { combineReducers } from 'redux';
import { permittedRolesReducer } from './permittedRoles';
import { departmentsReducer } from './departments';
import { isAuthenticatedReducer } from './isAuthenticated';

const allReducers = combineReducers({
    permittedRoles  : permittedRolesReducer,
    departments     : departmentsReducer,
    isAuthenticated : isAuthenticatedReducer
});

export default allReducers;

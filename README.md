
# InsureMe.LK Test
Sudesh Dayananda
sudesh.ryan@gmail.com

## Client

To run the client site: 

1. `cd` into `client` folder
2. Run command 
```sh
npx serve build
```

3. Site will be on `http://localhost:5000/` by default

##

## Server

To start the server, follow these steps:

1. `cd` into `server` folder
2. Run command 
```
npm install
```
3. Rename `.env.example` file to `.env`
4. Change MongoDB server config data in `.env` file
```
DB_HOST=localhost
DB_PORT=27017
DB_DATABASE=insureme_test
DB_USER=
DB_PASSWORD=
``` 
5. To seed the database, run:
```
npm run db-seed
```
6. To run server:
```
npm start
```
##

## Notes

User accounts to login:

| Email                     | Password    | Role      |
| :--------                 | :-------    | :-------- |
| `admin@insureme.test`     | `admin`     | Admin     |
| `manager@insureme.test`   | `manager`   | Manager   |
| `team_lead@insureme.test` | `team_lead` | Team Lead |